#include <iostream>
#include <cmath>
#include <chrono>

using namespace std::chrono;

__global__
void add(int n, float* x, float* y) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (int i = index; i < n; i += stride) {
        y[i] = x[i] + y[i];
    }
}


int main() {
    int N = 1 << 28;
    size_t size = N * sizeof(float); // размер массивов в байтах
    float *d_x, *d_y;
    float *h_x, *h_y;

    auto start = high_resolution_clock::now();

    //cudaSetDevice(0);

    // выделения памяти на CPU и GPU
    h_x = (float*)malloc(size);
    h_y = (float*)malloc(size);
    cudaMalloc(&d_x, size);
    cudaMalloc(&d_y, size);


    for (int i = 0; i < N; ++i) {
        h_x[i] = 1.0f;
        h_y[i] = 2.0f;
    }


    cudaMemcpy(d_x, h_x, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_y, h_y, size, cudaMemcpyHostToDevice);

    int blockSize = 256;

    int numBlocks = (N + blockSize - 1) / blockSize;

    auto start_comp = high_resolution_clock::now();

    add<<<numBlocks, blockSize>>>(N, d_x, d_y);

    //cudaDeviceSynchronize();
    cudaMemcpy(h_y, d_y, size, cudaMemcpyDeviceToHost);

    auto stop_comp = high_resolution_clock::now();
    auto duration_comp = duration_cast<milliseconds>(stop_comp - start_comp);

    float maxError = 0.0f;
    for (int i = 0; i < N; i++) {
        maxError = fmax(maxError, fabs(h_y[i]-3.0f));
    }

    cudaFree(d_x);
    cudaFree(d_y);
    free(h_x);
    free(h_y);

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);

    std::cout << "Max error: " << maxError << " duration: " << duration_comp.count() << "/" << duration.count() << "ms"  << std::endl;

    return 0;
}
