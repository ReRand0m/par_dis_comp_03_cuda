#include <iostream>
#include <cmath>
#include <chrono>

using namespace std::chrono;

__global__
void add(int n, float* x, float* y) {
    int index = threadIdx.x;
    int stride = blockDim.x;

    for (int i = index; i < n; i += stride) {
        y[i] = x[i] + y[i];
    }
}


int main() {
    int N = 1 << 28;
    float *x, *y;

    auto start = high_resolution_clock::now();

    //cudaSetDevice(0);

    cudaMallocManaged(&x, N * sizeof(float));
    cudaMallocManaged(&y, N * sizeof(float));

    for (int i = 0; i < N; ++i) {
        x[i] = 1.0f;
        y[i] = 2.0f;
    }

    auto start_comp = high_resolution_clock::now();

    add<<<1, 256>>>(N, x, y);

    cudaDeviceSynchronize();

    auto stop_comp = high_resolution_clock::now();
    auto duration_comp = duration_cast<milliseconds>(stop_comp - start_comp);

    float maxError = 0.0f;
    for (int i = 0; i < N; i++) {
        maxError = fmax(maxError, fabs(y[i]-3.0f));
    }

    cudaFree(x);
    cudaFree(y);

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);

    std::cout << "Max error: " << maxError << " duration: " << duration_comp.count() << "/" << duration.count() << "ms"  << std::endl;

    return 0;
}
